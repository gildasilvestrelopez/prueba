import {Component} from '@angular/core';

import {OAuthService} from '../../services/oauth.service';

@Component({
  selector: 'mr-login',
  templateUrl: './mr-login.component.html',
  styleUrls: ['./mr-login.component.scss']
})
export class MrLoginComponent implements OnInit {
  public isLoggedIn: boolean = false;

  constructor(private _oAuthService: OAuthService) {}

  public ngOnInit(): void {
    this._oAuthService.handleRedirectCallback();
  }

  public login(): void {
    this._oAuthService.login();
  }
}
