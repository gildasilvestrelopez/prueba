/* eslint-disable @typescript-eslint/naming-convention */
import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CustomElement} from '@set-social-services/common-custom-element';
import {takeUntil} from 'rxjs/operators';
import {IframeLoaderService, SpinnerService} from '@set/social-tags-commons';
import {Subject} from 'rxjs';

@Component({
  selector: 'ati-view-changes-yd',
  templateUrl: './view-changes.component.html',
  styleUrls: ['./view-changes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@CustomElement('ati-view-changes-yd')
export class ViewChangesComponent implements OnInit, OnDestroy {
  @Input() public data: {diffChanges: string} | undefined;
  private _iframe?: HTMLIFrameElement;
  private _destroySubject: Subject<void>;

  constructor(
    private _spinnerService: SpinnerService,
    private _iframeLoaderService: IframeLoaderService
  ) {
    this._destroySubject = new Subject();
  }

  public ngOnInit(): void {
    debugger
    this._spinnerService.setLoading(true);
    this._iframeLoaderService
      .getLoaded()
      .pipe(takeUntil(this._destroySubject))
      .subscribe(iframe => {
        this._iframe = iframe;
        setTimeout(() => {
          this._removeElements();
          this._spinnerService.setLoading(false);
        }, 1000);
      });
  }

  public ngOnDestroy(): void {
    this._destroySubject.next();
    this._destroySubject.complete();
  }

}